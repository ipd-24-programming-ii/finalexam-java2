package question5Message;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class EmailMessage extends Message implements ISendInfo {

	public EmailMessage(EmailUser receiver, EmailUser sender, String body) {
		super(receiver, sender, body);
	}
	

	
	@Override
	public String toString() {
		return "EmailMessage [toString()=" + super.toString() + "]";
	}


	@Override
    public boolean validateMessage(User sender, User receiver, String body){
		EmailUser thisSender = (EmailUser) sender;
		EmailUser thisReceiver = (EmailUser) receiver;
		if (thisSender.notContainsMandatoryChar()) {
			throw new IllegalArgumentException("Email sender does not contain @ or .");
		}
		if (thisReceiver.notContainsMandatoryChar()) {
			throw new IllegalArgumentException("Email receiver does not contain @ or .");
		}
        if(body == null || body.isEmpty()) {
            throw new IllegalArgumentException("Email message body should not be empty");
        }
        if(body.contains("^") || body.contains("*") || body.contains("!")) {
            throw new IllegalArgumentException("Email message body should not contain ^ or * or !");
        }
        return true;
    }

	@Override
	public void sendMessage(Message message) throws IOException {
		try (FileWriter fileWriter = new FileWriter("src/question5Message/resource/email.txt")){
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.print(message.toString());
		}catch(IOException e) {
			throw e;
		}
		System.out.println("all email information has been saved in the email folder successfully!"+"\n");
	}
	
}
