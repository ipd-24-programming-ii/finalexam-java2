package question5Message;

public class EmailUser extends User{
	private String emailAddress;

	public EmailUser(String firstName, String lastName, Address address, String emailAddress) {
		super(firstName, lastName, address);
		this.emailAddress = emailAddress;
	}
	boolean notContainsMandatoryChar() {
		if(!this.emailAddress.contains("@") || !this.emailAddress.contains(".")) {
            return true;
        }
		return false;
	}
}
