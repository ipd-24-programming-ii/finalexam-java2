package question5Message;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class SmsMessage extends Message  implements ISendInfo{

	public SmsMessage(User receiver, User sender, String body) {
		super(receiver, sender, body);
	}

	@Override
    public boolean validateMessage(User sender, User receiver, String body){
		SmsUser thisSender = (SmsUser) sender;
		SmsUser thisReceiver = (SmsUser) receiver;
        if(!thisSender.containsValidPhoneNumber()) {
            throw new IllegalArgumentException("Sms sender phone number should have 10 digits");
        }
        if(!thisReceiver.containsValidPhoneNumber()) {
            throw new IllegalArgumentException("Sms receiver phone number should have 10 digits");
        }
        if(body.isEmpty()) {
            throw new IllegalArgumentException("Sms message body should not be empty");
        }
        if(body.length() > 160) {
            throw new IllegalArgumentException("Sms message body should not longer than 160");
        }
        if(body.contains("&") || body.contains("#") || body.contains("@")) {
            throw new IllegalArgumentException("Sms message body should not contain & or # or @");
        }
        return true;
    }		

	@Override
	public void sendMessage(Message message) throws IOException {
		try (FileWriter fileWriter = new FileWriter("src/question5Message/resource/sms.txt")){
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.print(message.toString());
		}catch(IOException e) {
			throw e;
		}
		System.out.println("all sms information has been saved in the sms folder successfully!"+"\n");
	}		
	
}
	

