package question5Message;

public class SmsUser extends User{
	
	private String phoneNumber;

	public SmsUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName, address);
		setPhoneNumber(phoneNumber);
	}

	public void setPhoneNumber(String phoneNumber) {
        if(phoneNumber.matches("\\d+")) {
            this.phoneNumber = phoneNumber;
        }
        else {
            throw new IllegalArgumentException("phone number contains non-numeric");
        }
    }
	
	boolean containsValidPhoneNumber() {
		if(this.phoneNumber.matches("\\d{10}")) {
            return true;
        }
		return false;
	}
}
