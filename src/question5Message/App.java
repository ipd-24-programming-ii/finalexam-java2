package question5Message;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		List<Message> listOfMessages = new ArrayList <Message>();
		
		EmailMessage email = new EmailMessage(
				new EmailUser("Judy","Foster", new Address("Main Street",1), "a.b@g.com"),
				new EmailUser("Betty", "Beans", new Address("Second Street",2), "v.r@g.com"),
				"This is one emial");
	
		SmsMessage smsMessage = new SmsMessage(
				new SmsUser("Judy","Foster", new Address("Main Street",1), "1234567890"),
				new SmsUser("Betty", "Beans", new Address("Second Street",2), "1234567890"),
				"This is one sms");
		
		listOfMessages.add(email);
		listOfMessages.add(smsMessage);
		
		for (Message message : listOfMessages) {			
			if (message instanceof EmailMessage) {
				if(((EmailMessage) message).validateMessage(message.getSender(),message.getReceiver(), message.getBody())) {
					try {
						((EmailMessage) message).sendMessage(message);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			if (message instanceof SmsMessage) {
				if(((SmsMessage) message).validateMessage(message.getSender(),message.getReceiver(), message.getBody())) {
					try {
						((SmsMessage) message).sendMessage(message);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
