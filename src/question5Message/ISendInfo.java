package question5Message;

import java.io.IOException;

public interface ISendInfo {
	
	boolean validateMessage(User sender, User receiver, String body);
	
	void sendMessage(Message message)throws IOException;
	
}
